import FlightsService, {
  Flight,
  FlightsQueryParams,
} from '../src/flightsService';

const queryParams: FlightsQueryParams = new FlightsQueryParams('lot');

describe('flights service', () => {
  it('get status number by provided color', () => {
    const statusOnTime = FlightsService.getStatusNumber('Green');
    expect(statusOnTime).toBe(1);

    const statusShortDelay = FlightsService.getStatusNumber('Yellow');
    expect(statusShortDelay).toBe(2);

    const statusLongDelay = FlightsService.getStatusNumber('Red');
    expect(statusLongDelay).toBe(3);

    const statusNotKnown1 = FlightsService.getStatusNumber('');
    expect(statusNotKnown1).toBe(0);

    const statusNotKnown2 = FlightsService.getStatusNumber('9sd8fdn30djkfh');
    expect(statusNotKnown2).toBe(0);
  });
  it('verify calculated areas', () => {
    const [area1OffsetX, area1OffsetY] = [180, 90];
    const areas1 = FlightsService.calculateMapAreas(area1OffsetX, area1OffsetY);
    expect(areas1.length).toBe(4);

    const [area2OffsetX, area2OffsetY] = [90, 90];
    const areas2 = FlightsService.calculateMapAreas(area2OffsetX, area2OffsetY);
    expect(areas2.length).toBe(8);

    const [area3OffsetX, area3OffsetY] = [45, 45];
    const areas3 = FlightsService.calculateMapAreas(area3OffsetX, area3OffsetY);
    expect(areas3.length).toBe(32);

    const verifyContent = (
      offsetX: number,
      offsetY: number,
      areas: number[][]
    ) => {
      let index = 0;
      for (let y: number = -90; y <= 90 - offsetY; y += offsetY) {
        for (let x: number = -180; x <= 180 - offsetX; x += offsetX) {
          // mapAreas.push([y + offsetY, y, x, x + offsetX]);
          expect(areas[index][0]).toBe(y + offsetY);
          expect(areas[index][1]).toBe(y);
          expect(areas[index][2]).toBe(x);
          expect(areas[index][3]).toBe(x + offsetX);
          index++;
        }
      }
    };

    verifyContent(area1OffsetX, area1OffsetY, areas1);
    verifyContent(area2OffsetX, area2OffsetY, areas2);
    verifyContent(area3OffsetX, area3OffsetY, areas3);
  });
  it('get flights from specified area', async () => {
    // Poland area
    // https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=54.81,48.59,10.54,25.08&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1
    // area [lat end, lat start, lon start, lon end]
    const area: number[] = [54, 48, 10, 25];
    const flightsData: any = await FlightsService.getFrMapAreaData(
      area,
      new FlightsQueryParams()
    );
    const flightsList: Flight[] = FlightsService.getFlightsList(flightsData);

    expect(flightsList.length).toBeGreaterThan(0);

    flightsList.forEach(flight => {
      expect(flight.latitude).toBeLessThanOrEqual(area[0]);
      expect(flight.latitude).toBeGreaterThanOrEqual(area[1]);
      expect(flight.longitude).toBeLessThanOrEqual(area[3]);
      expect(flight.longitude).toBeGreaterThanOrEqual(area[2]);
    });
  });
  it('compare LOT flights count with changed map areas', async () => {
    const mapAreasData1 = await FlightsService.getFrMapAreasData(
      FlightsService.calculateMapAreas(90, 90),
      queryParams
    );
    const flightsList1 = await FlightsService.getFlightsList(mapAreasData1);

    const mapAreasData2 = await FlightsService.getFrMapAreasData(
      FlightsService.calculateMapAreas(360, 180),
      queryParams
    );
    const flightsList2 = await FlightsService.getFlightsList(mapAreasData2);

    expect(flightsList1.length).toBeGreaterThan(0);
    expect(flightsList2.length).toBeGreaterThan(0);
    expect(flightsList1.length).toBe(flightsList2.length);
  });
  it('compare British Airways flights count with changed map areas', async () => {
    const bawQueryParams = new FlightsQueryParams('baw');
    const mapAreasData1 = await FlightsService.getFrMapAreasData(
      FlightsService.calculateMapAreas(90, 90),
      bawQueryParams
    );
    const flightsList1 = await FlightsService.getFlightsList(mapAreasData1);

    const mapAreasData2 = await FlightsService.getFrMapAreasData(
      FlightsService.calculateMapAreas(360, 180),
      bawQueryParams
    );
    const flightsList2 = await FlightsService.getFlightsList(mapAreasData2);

    expect(flightsList1.length).toBeGreaterThan(0);
    expect(flightsList2.length).toBeGreaterThan(0);
    expect(flightsList1.length).toBe(flightsList2.length);
  });
  xit('get flights list and details', async () => {
    const result: Flight[] = await FlightsService.getAll(queryParams);
    expect(result.length).toBeGreaterThan(0);
    result.forEach(fl => {
      const callsignOk: boolean =
        fl.callsign === '' || fl.callsign.toLowerCase().startsWith('lo');
      expect(fl.details).toBeUndefined();
      expect(callsignOk).toBeTruthy();
    });

    const resultWithDetails = await FlightsService.getFlightDetailsAllWithRetries(
      result
    );
    expect(resultWithDetails.length).toBeGreaterThan(0);
    resultWithDetails.forEach(fl => {
      const details = fl.details;
      expect(details).toBeDefined();
    });
  }, 30000);
});
