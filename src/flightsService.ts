import request from 'request';

interface IFlightsQueryParams {
  callsign: string;
}

// tslint:disable-next-line: max-classes-per-file
class FlightsQueryParams {
  public callsign: string;
  constructor(callsignParam: string = '') {
    this.callsign = callsignParam || '';
  }
}

interface IFlightNumber {
  default: string;
  alternative?: string; // nullable
}

// tslint:disable-next-line: max-classes-per-file
class FlightNumber {
  public default: string;
  public alternative?: string; // nullable
  constructor() {
    this.default = '';
  }
}

interface IFlightIdentification {
  id: string;
  row: number;
  number: IFlightNumber;
  callsign: string;
}

// tslint:disable-next-line: max-classes-per-file
class FlightIdentification {
  public id: string;
  public row: number;
  public number: IFlightNumber;
  public callsign: string;
  constructor() {
    this.id = '';
    this.row = -1;
    this.number = new FlightNumber();
    this.callsign = '';
  }
}

interface IFlightStatusGenericStatus {
  text: string; // np estimated, landed
  color: string; // red, yellow, green
  diverted?: string; // gdzie przekierowany (opcjonalne property)
  type: string; // np arrival
}

// tslint:disable-next-line: max-classes-per-file
class FlightStatusGenericStatus {
  public text: string; // np estimated, landed
  public color: string; // red, yellow, green
  public diverted?: string; // gdzie przekierowany (opcjonalne property)
  public type: string; // np arrival
  constructor() {
    this.text = '';
    this.color = '';
    this.type = '';
  }
}

interface IFlightStatusEventTime {
  utc: number;
  local: number;
}

// tslint:disable-next-line: max-classes-per-file
class FlightStatusEventTime {
  public utc: number;
  public local: number;
  constructor() {
    this.utc = -1;
    this.local = -1;
  }
}

interface IFlightStatusGeneric {
  status: IFlightStatusGenericStatus;
  eventTime?: IFlightStatusEventTime;
}

// tslint:disable-next-line: max-classes-per-file
class FlightStatusGeneric {
  public status: IFlightStatusGenericStatus;
  public eventTime?: IFlightStatusEventTime;
  constructor() {
    this.status = new FlightStatusGenericStatus();
  }
}

interface IFlightStatus {
  live: boolean;
  text: string;
  icon: string;
  // estimated?:
  ambiguous: boolean;
  generic: IFlightStatusGeneric;
}

// tslint:disable-next-line: max-classes-per-file
class FlightStatus {
  public live: boolean;
  public text: string;
  public icon: string;
  public ambiguous: boolean;
  public generic: IFlightStatusGeneric;
  constructor() {
    this.live = false;
    this.text = '';
    this.icon = '';
    this.ambiguous = false;
    this.generic = new FlightStatusGeneric();
  }
}

interface IAircraftModel {
  code: string;
  text: string;
}

// tslint:disable-next-line: max-classes-per-file
class AircraftModel {
  public code: string;
  public text: string;
  constructor() {
    this.code = '';
    this.text = '';
  }
}

interface IAircraft {
  model: IAircraftModel;
  countryId: number;
  registration: string;
  images: object;
}

// tslint:disable-next-line: max-classes-per-file
class Aircraft {
  public model: IAircraftModel;
  public countryId: number;
  public registration: string;
  public images: object;
  constructor() {
    this.model = new AircraftModel();
    this.countryId = -1;
    this.registration = '';
    this.images = new Object();
  }
}

interface IAirlineCode {
  iata: string;
  icao: string;
}

// tslint:disable-next-line: max-classes-per-file
class AirlineCode {
  public iata: string;
  public icao: string;
  constructor() {
    this.iata = '';
    this.icao = '';
  }
}

interface IAirline {
  name: string;
  short: string;
  code: IAirlineCode;
  url: string;
}

// tslint:disable-next-line: max-classes-per-file
class Airline {
  public name: string;
  public short: string;
  public code: IAirlineCode;
  public url: string;
  constructor() {
    this.name = '';
    this.short = '';
    this.code = new AirlineCode();
    this.url = '';
  }
}

interface IAirportCode {
  iata: string;
  icao: string;
}

// tslint:disable-next-line: max-classes-per-file
class AirportCode {
  public iata: string;
  public icao: string;
  constructor() {
    this.iata = '';
    this.icao = '';
  }
}

interface IAirportCountry {
  name: string;
  code: string;
  codeLong: string;
}

// tslint:disable-next-line: max-classes-per-file
class AirportCountry {
  public name: string;
  public code: string;
  public codeLong: string;
  constructor() {
    this.name = '';
    this.code = '';
    this.codeLong = '';
  }
}

interface IAirportRegion {
  city: string;
}

// tslint:disable-next-line: max-classes-per-file
class AirportRegion {
  public city: string;
  constructor() {
    this.city = '';
  }
}

interface IAirportPosition {
  latitude: number;
  longitude: number;
  altitude: number;
  country: IAirportCountry;
  region: IAirportRegion;
}

// tslint:disable-next-line: max-classes-per-file
class AirportPosition {
  public latitude: number;
  public longitude: number;
  public altitude: number;
  public country: IAirportCountry;
  public region: IAirportRegion;
  constructor() {
    this.latitude = -1;
    this.longitude = -1;
    this.altitude = -1;
    this.country = new AirportCountry();
    this.region = new AirportRegion();
  }
}

interface IAirportTimezone {
  name: string;
  offset: number;
  offsetHours: string;
  abbr: string;
  abbrName: string;
  isDst: boolean;
}

// tslint:disable-next-line: max-classes-per-file
class AirportTimezone {
  public name: string;
  public offset: number;
  public offsetHours: string;
  public abbr: string;
  public abbrName: string;
  public isDst: boolean;
  constructor() {
    this.name = '';
    this.offset = -1;
    this.offsetHours = '';
    this.abbr = '';
    this.abbrName = '';
    this.isDst = false;
  }
}

interface IAirportData {
  name: string;
  code: IAirportCode;
  position: IAirportPosition;
  timezone: IAirportTimezone;
  visible: boolean;
  website: string;
}

// tslint:disable-next-line: max-classes-per-file
class AirportData {
  public name: string;
  public code: IAirportCode;
  public position: IAirportPosition;
  public timezone: IAirportTimezone;
  public visible: boolean;
  public website: string;
  constructor() {
    this.name = '';
    this.code = new AirportCode();
    this.position = new AirportPosition();
    this.timezone = new AirportTimezone();
    this.visible = false;
    this.website = '';
  }
}

interface IAirport {
  origin: IAirportData;
  destination: IAirportData;
  real?: IAirportData; // w przypadku diverta dodatkowe airport real
}

// tslint:disable-next-line: max-classes-per-file
class Airport {
  public origin: IAirportData;
  public destination: IAirportData;
  public real?: IAirportData; // w przypadku diverta dodatkowe airport real
  constructor() {
    this.origin = new AirportData();
    this.destination = new AirportData();
  }
}

interface IFlightTimeInfo {
  departure?: number;
  arrival?: number;
}

// tslint:disable-next-line: max-classes-per-file
class FlightTimeInfo {
  public departure?: number;
  public arrival?: number;
}

interface IFlightTimeOther {
  eta?: number;
  updated?: number;
}

// tslint:disable-next-line: max-classes-per-file
class FlightTimeOther {
  public eta?: number;
  public updated?: number;
}

interface IFlightTimeHistorical {
  flighttime?: string;
  delay?: string;
}

// tslint:disable-next-line: max-classes-per-file
class FlightTimeHistorical {
  public flighttime?: string;
  public delay?: string;
}

interface IFlightTime {
  scheduled: IFlightTimeInfo;
  real: IFlightTimeInfo;
  estimated: IFlightTimeInfo;
  other: IFlightTimeOther;
  historical: IFlightTimeHistorical;
}

// tslint:disable-next-line: max-classes-per-file
class FlightTime {
  public scheduled: IFlightTimeInfo;
  public real: IFlightTimeInfo;
  public estimated: IFlightTimeInfo;
  public other: IFlightTimeOther;
  public historical: IFlightTimeHistorical;
  constructor() {
    this.scheduled = new FlightTimeInfo();
    this.real = new FlightTimeInfo();
    this.estimated = new FlightTimeInfo();
    this.other = new FlightTimeOther();
    this.historical = new FlightTimeHistorical();
  }
}

interface IFlightDetails {
  identification: IFlightIdentification;
  status: IFlightStatus;
  aircraft: IAircraft;
  airline: IAirline;
  airport: IAirport;
  time: IFlightTime;
}

// tslint:disable-next-line: max-classes-per-file
class FlightDetails {
  public identification: IFlightIdentification;
  public status: IFlightStatus;
  public aircraft: IAircraft;
  public airline: IAirline;
  public airport: IAirport;
  public time: IFlightTime;
  constructor() {
    this.identification = new FlightIdentification();
    this.status = new FlightStatus();
    this.aircraft = new Aircraft();
    this.airline = new Airline();
    this.airport = new Airport();
    this.time = new FlightTime();
  }
}

interface IFlight {
  id: string;
  callsign: string;
  from: string;
  to: string;
  latitude: number;
  longitude: number;
  track: number;
  altitude: number;
  speed: number;
  aircraft: string;
  reg: string;
  details: IFlightDetails;
}

// tslint:disable-next-line: max-classes-per-file
class Flight implements IFlight {
  public id: string;
  public callsign: string;
  public from: string;
  public to: string;
  public latitude: number;
  public longitude: number;
  public track: number;
  public altitude: number;
  public speed: number;
  public aircraft: string;
  public reg: string;
  public details: FlightDetails;

  constructor() {
    this.id = '';
    this.callsign = '';
    this.from = '';
    this.to = '';
    this.latitude = -1;
    this.longitude = -1;
    this.track = -1;
    this.altitude = -1;
    this.speed = -1;
    this.aircraft = '';
    this.reg = '';
    this.details = new FlightDetails();
  }
}

interface IGetDetailsAllResult {
  errors: number;
  flights: IFlight[];
}

// tslint:disable-next-line: max-classes-per-file
class FlightsService {
  public static getStatusNumber(statusColor: string): number {
    let status = 0;
    switch (statusColor.toLowerCase()) {
      case 'green':
        status = 1;
        break;
      case 'yellow':
        status = 2;
        break;
      case 'red':
        status = 3;
        break;
      default:
        status = 0;
    }
    return status;
  }

  public static async getAll(
    queryParams: IFlightsQueryParams,
    func?: (fl: any) => any
  ): Promise<IFlight[]> {
    const mapAreasData = await this.getFrMapAreasData(
      this.calculateMapAreas(360, 180),
      queryParams
    );
    return this.getFlightsList(mapAreasData, func);
  }

  public static getFlightDetails(flightId: string): Promise<IFlightDetails> {
    return new Promise(async (resolve, reject) => {
      const detailsUrl: string = `https://data-live.flightradar24.com/clickhandler/?version=1.5&flight=${flightId}`;
      request(
        detailsUrl,
        { json: true },
        (err: Error, res: request.Response, body: any) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(body as IFlightDetails);
        }
      );
    });
  }

  /**
   *
   * @param flights
   */
  public static getFlightDetailsAll(
    flights: IFlight[]
  ): Promise<IGetDetailsAllResult> {
    return new Promise(async (resolve, reject) => {
      let counter = 0;
      let errors = 0;
      for (const flight of flights) {
        try {
          const details: any = await this.getFlightDetails(flight.id);
          counter++;
          if (this.isObject(details)) {
            delete details.flightHistory;
            delete details.trail;
            flight.details = details;
          } else {
            errors++;
          }
          if (counter >= flights.length) {
            resolve({ errors, flights } as IGetDetailsAllResult);
          }
        } catch (err) {
          reject(err);
        }
      }
    });
  }

  /**
   * Gets flight details with retrying on errors
   * @param flights
   */
  public static getFlightDetailsAllWithRetries(
    flights: IFlight[]
  ): Promise<IFlight[]> {
    return new Promise(async (resolve, reject) => {
      const retriesCount: number = 15;
      const retriesSleep: number = 2000;
      let flightsToGet: IFlight[] = flights;

      for (let i = 1; i <= retriesCount; i++) {
        try {
          const getAllResult = await this.getFlightDetailsAll(flightsToGet);
          if (getAllResult.errors > 0) {
            await this.sleep(retriesSleep);
            flightsToGet = getAllResult.flights.filter(
              (flight: any) => flight.details === undefined
            );
          } else {
            resolve(flights as IFlight[]);
            break;
          }
          if (i === retriesCount) {
            reject(new Error('Too many errors on getting details.'));
          }
        } catch (err) {
          // TODO logowanie bledu pobierania details
          // console.log(err);
        }
      }
    });
  }

  /**
   * Calculates map areas
   * @param offsetX
   * @param offsetY
   */
  public static calculateMapAreas(
    offsetX: number,
    offsetY: number
  ): number[][] {
    const mapAreas: number[][] = [];
    for (let y: number = -90; y <= 90 - offsetY; y += offsetY) {
      for (let x: number = -180; x <= 180 - offsetX; x += offsetX) {
        mapAreas.push([y + offsetY, y, x, x + offsetX]);
      }
    }
    return mapAreas;
  }

  /**
   * Returns flights list as object. Key is flight id, value is array of data to parse
   * @param mapArea Array of rectangle coordinates
   * @param queryParams
   */
  public static getFrMapAreaData(
    mapArea: number[],
    queryParams: IFlightsQueryParams
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let zoneUrl: string = `https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=${mapArea[0]},${mapArea[1]},${mapArea[2]},${mapArea[3]}&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1`;
      if (queryParams.callsign) {
        zoneUrl += `&callsign=${queryParams.callsign.toUpperCase()}`;
      } else {
        // odblokowana mozliwosc pobierania wszystkich lotow w libce !!!
        // reject(new Error('GetFrZoneData - callsign not provided.'));
      }
      request(
        zoneUrl,
        { json: true },
        (err: Error, res: request.Response, body: any) => {
          if (err) {
            reject(err);
            return;
          }
          delete body.full_count;
          delete body.version;
          delete body.stats;
          delete body['selected-aircraft'];

          resolve(body);
        }
      );
    });
  }

  /**
   * Returns Object. Key is flight id, value is flight data as array
   * @param mapAreasArr
   * @param queryParams
   */
  public static getFrMapAreasData(
    mapAreasArr: number[][],
    queryParams: IFlightsQueryParams
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const frMapAreasPromises: any[] = [];
      mapAreasArr.forEach(mapArea => {
        frMapAreasPromises.push(this.getFrMapAreaData(mapArea, queryParams));
      });
      const allMapAreasDataArr: any[] = await Promise.all(frMapAreasPromises);
      const allMapAreasData = Object.assign({}, ...allMapAreasDataArr);
      resolve(allMapAreasData);
    });
  }

  /**
   * Returns flights array
   * @param mapAreaData Object. Key is flight id, value is flight data as array
   * @param func Callback function to execute on each data record
   */
  public static getFlightsList(
    mapAreaData: any,
    func?: (fl: any) => any
  ): IFlight[] {
    const flights: IFlight[] = [];
    Object.keys(mapAreaData).forEach((key: string) => {
      const val: any[] = mapAreaData[key];
      if (Array.isArray(val) && val.length >= 14) {
        let flight = {
          aircraft: val[8],
          altitude: val[4],
          callsign: val[13],
          from: val[11],
          id: key,
          latitude: val[1],
          longitude: val[2],
          reg: val[9],
          speed: val[5],
          to: val[12],
          track: val[3],
        };
        if (func) {
          flight = func(flight);
        }
        flights.push(flight as IFlight);
      }
    });
    return flights;
  }

  // helpers

  private static isObject(obj: any): boolean {
    return (
      (typeof obj === 'object' && obj !== null) || typeof obj === 'function'
    );
  }

  private static sleep(milliseconds: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  // private log(message: string): void {
  //   console.log(message);
  // }
}
export default FlightsService;
export { Flight, AirportData, FlightsQueryParams };
