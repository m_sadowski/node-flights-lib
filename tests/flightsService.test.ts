import { expect } from 'chai';
import FlightsService, {
  Flight,
  FlightsQueryParams,
} from '../src/flightsService';

const queryParams: FlightsQueryParams = new FlightsQueryParams('lot');

describe('Flights service', () => {
  describe('flights from area', () => {
    it('list and details', async () => {
      const result: Flight[] = await FlightsService.getAll(queryParams);
      expect(result).length.greaterThan(0);
      result.forEach(fl => {
        const callsignOk: boolean =
          fl.callsign === '' || fl.callsign.toLowerCase().startsWith('lo');
        expect(fl.details).to.be.an('undefined');
        expect(callsignOk).eq(true);
      });

      const resultWithDetails = await FlightsService.getFlightDetailsAllWithRetries(
        result
      );
      expect(resultWithDetails).length.greaterThan(0);
      resultWithDetails.forEach(fl => {
        const details = fl.details;
        expect(details).not.to.be.an('undefined');
      });
    });
  });
});
